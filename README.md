This is **NOT** the original repository of the xdmf lib.

It contains modification of the original source code. Do not use this repository outside of https://www.opengeosys.org/

The original source can be found here: https://gitlab.kitware.com/xdmf/xdmf 

The original webpage is: http://www.xdmf.org

   
